package com.orange.devdays.client.dummy;

import com.beust.jcommander.Parameter;

/**
 * Class that describes the application command line parameters parsed by jcommander.
 * @author cbtv4480
 *
 */
public class AppArgs {

	@Parameter(names = { "-u", "-user" }, description = "user name", required = true)
	String userName;

	@Parameter(names = { "-p", "-password" }, description = "password", required = true)
	String password;

	@Parameter(names = { "-t", "-team" }, description = "Team name", required = true)
	String teamName;

	@Parameter(names = { "-gn", "-ghostName" }, description = "Ghost name", required = true)
	String ghostName;

	@Parameter(names = { "-pn", "-pacmanName" }, description = "Pacman name", required = true)
	String pacmanName;

	@Parameter(names = { "-url", "-gameURL" }, description = "Server server URL", required = true)
	String gameServerURL;

	@Parameter(names = { "-tid", "-tournamentId" }, description = "Tournament identifier", required = true)
	String tournamentId;

}
