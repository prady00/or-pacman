package com.orange.devdays.client.dummy;

import org.apache.log4j.Logger;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;

import devdays.client.GameClient;
import devdays.model.common.GameStatus;
import devdays.model.common.TournamentStatus;
import devdays.model.service.GameDto;
import devdays.model.service.TournamentDto;

/**
 * Main class
 * @author cbtv4480
 *
 */
public class Main {
	private static final Logger logger = Logger.getLogger(Main.class);

	public static void main(String[] args) {

		AppArgs appArgs = new AppArgs();
		parseArgs(args, appArgs);

		GameClient client = new GameClient(appArgs.gameServerURL);

		//
		// 1 - Login on server
		//
		client.login(appArgs.userName, appArgs.password);

		//
		// 2 - Register team in tournement
		//
		logger.info("Registering in tournament");
		client.registerInTournament(appArgs.tournamentId, appArgs.teamName, appArgs.pacmanName, appArgs.ghostName);

		while (true) {

			GameDto nextGameInTournament = null;
			while (nextGameInTournament == null) {
				logger.info("Waiting for a game from tournament " + appArgs.tournamentId + " ...");
				try {
					//
					// 3 - Wait for a game to be available
					// => all team have joined
					// => the tournament turn is ready
					//
					nextGameInTournament = client.waitTournamentGameAvailable(appArgs.tournamentId, appArgs.teamName);
				} catch (Exception e) {
					logger.error("Error", e);
				}
			}

			//
			// Register each player in game
			//
			logger.info("Joining game " + nextGameInTournament.getId());
			
			// 4.1 - my Pacman joins the game
			client.registerInGame(appArgs.pacmanName, nextGameInTournament.getId());
			
			// 4.2 - my Fantom joins the game
			client.registerInGame(appArgs.ghostName, nextGameInTournament.getId());

			//
			// 5 - Wait all players join the game
			//
			GameDto game = null;
			while (game == null) {
				logger.info("Waiting for other players...");
				try {
					game = client.waitGameStatusChange(nextGameInTournament.getId(), GameStatus.WAITING_FOR_PLAYER);
				} catch (Exception e) {
					// All players are not ready
					logger.error("Error", e);
				}
			}

			logger.info("All players joined the game");

			Player fantom = new Player(client, appArgs.ghostName, appArgs.teamName, true, nextGameInTournament.getId());
			Player pacman = new Player(client, appArgs.pacmanName, appArgs.teamName, false,
					nextGameInTournament.getId());

			logger.info("GO!");

			// 6 - Let's play this game 
			fantom.start();
			pacman.start();

			try {
				// 7 - Wait end of game
				fantom.join();
				pacman.join();

				logger.info("End of game");
				game = client.waitGameStatusChange(nextGameInTournament.getId(), GameStatus.PLAYING);

				logger.info("The winning team is: " + game.getWinningTeam());
			} catch (Exception e) {
				logger.error("Error", e);
			}

			// 8 - Test if tournament is finished
			TournamentDto tournament = client.getTournament(appArgs.tournamentId);
			if (tournament.getStatus() == TournamentStatus.FINISHED) {
				logger.info("Tournament FINISHED - The winner is " + tournament.getWinningTeam());
				break;
			}
			else {
				logger.info("Tournament still running - an other game may be available");
			}

		}
	}

	/**
	 * Parse command line arguments
	 * @param args command line arguments
	 * @param appArgs args setted as properties
	 */
	private static void parseArgs(String[] args, AppArgs appArgs) {
		JCommander jc = null;
		try {
			jc = new JCommander(appArgs);
			jc.parse(args);

		} catch (ParameterException e) {
			jc.usage();

			e.printStackTrace();
			System.exit(-1);
		}
	}

}
