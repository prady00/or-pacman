package com.orange.devdays.client.dummy;

import java.util.ArrayList;
import java.util.Random;

import org.apache.log4j.Logger;

import devdays.client.GameClient;
import devdays.model.common.Move;
import devdays.model.service.PlayerTurnDataDto;
import devdays.model.service.PositionDto;
import devdays.model.service.TeamTurnDataDto;
import devdays.model.service.TurnDto;

/**
 * Thread used to implement one player (Ghost or pacman) moves.
 * @author cbtv4480
 *
 */
public class Player extends Thread {
	private static final Logger logger = Logger.getLogger(Player.class);

	private GameClient client;
	private String playerName;
	private String gameName;
	private String teamName;
	private boolean isGhost;

	public Player(GameClient gc, String playerName, String teamName, boolean isGhost, String gameName) {
		this.client = gc;
		this.playerName = playerName;
		this.teamName = teamName;
		this.gameName = gameName;
		this.isGhost = isGhost;

		// Set thread Name (only for logs)
		setName(playerName);
	}

	/**
	 * Entry point of the thread
	 */
	@Override
	public void run() {

		// Manage connection lost
		int curTurn = client.getGame(gameName).getCurrentTurn();
		while (true) {

			try {
				logger.debug(" Playing turn " + curTurn);

				// Wait for the turn to get available
				TurnDto turn = client.waitTurnCreation(gameName, curTurn);

				PlayerTurnDataDto myDesc = getMyDescription(turn, isGhost);

				PositionDto myPos = myDesc.getPosition();
				char[][] grid = turn.getMap();

				if (turn.isLastTurn()) {
					// Last turn reached
					logger.debug("Last turn reached");
					break;
				} else {
					logger.debug("Playing turn " + curTurn);
					// Compute next move to apply
					Move nextMove = getNextMove(myPos, grid);

					client.playAturn(playerName, gameName, curTurn, nextMove);
				}

			} catch (Exception e) {
				// Something wrong happened...
				e.printStackTrace();
			} finally {
				// Increment next turn to play in any circumstances
				curTurn++;
			}
		}

	}

	/**
	 * Compute next move. All the logic should be coded somewhere like here.
	 * @param myPos player position
	 * @param grid description of the map
	 * @return next move to send to server
	 */
	private Move getNextMove(PositionDto myPos, char[][] grid) {
		// -----------------------------------------
		// TODO put your player logic here
		// -----------------------------------------

		if (isGhost) {
			// -----------------------------------------
			// TODO put your ghost player logic here
			// -----------------------------------------
		} else {
			// -----------------------------------------
			// TODO put your pacman player logic here
			// -----------------------------------------
		}

		ArrayList<Move> nextMoves = new ArrayList<Move>();

		//
		// Default logic : random play outside of walls
		//
		int curLine = myPos.getLine();
		int curColumn = myPos.getColumn();

		if (curColumn - 1 >= 0) {
			if (grid[curLine][curColumn - 1] != 'X') {
				nextMoves.add(Move.LEFT);
			}
		}

		if (curColumn + 1 < grid[curLine].length) {
			if (grid[curLine][curColumn + 1] != 'X') {
				nextMoves.add(Move.RIGHT);
			}
		}

		if (curLine - 1 >= 0) {
			if (grid[curLine - 1][curColumn] != 'X') {
				nextMoves.add(Move.DOWN);
			}
		}

		if (curLine + 1 < grid.length) {
			if (grid[curLine][curColumn] != 'X') {
				nextMoves.add(Move.UP);
			}
		}

		Random random = new Random();
		int movePos = random.nextInt(nextMoves.size());

		return nextMoves.get(movePos);
	}

	/**
	 * Extract description of a player
	 * @param turn the turn
	 * @param isGhost am i a ghost ?
	 * @return Turn descriptor
	 */
	private PlayerTurnDataDto getMyDescription(TurnDto turn, boolean isGhost) {
		for (TeamTurnDataDto t : turn.getTeams()) {
			if (t.getTeamName().equals(teamName)) {
				if (isGhost) {
					return t.getGhost();
				} else {
					return t.getPacman();
				}
			}
		}
		return null;
	}
}
