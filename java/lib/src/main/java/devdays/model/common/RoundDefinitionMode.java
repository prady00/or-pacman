package devdays.model.common;

/**
 * Definition mode for the rounds.
 * 
 * <li>AUTO => Server defines automatically the matchs of a round and start-it 
 * <li>MANUAL => Matchs created manually by tournament administrator
 * @author cbtv4480
 *
 */
public enum RoundDefinitionMode {
	MANUAL, AUTO;
}
