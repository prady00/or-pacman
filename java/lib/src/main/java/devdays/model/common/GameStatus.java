package devdays.model.common;

public enum GameStatus {

	WAITING_FOR_PLAYER, PLAYING, FINISHED, ERROR;

}
