package devdays.model.common;

public enum ScoreEvent {
	EAT_GUM, EAT_OTHER_PACMAN, EAT_MY_PACMAN, EAT_BONUS, EAT_OTHER_GHOST, EAT_MY_GHOST;
}
