package devdays.model.common;

/**
 * Possible move orders for a player
 * 
 */
public enum Move {

	UP, RIGHT, DOWN, LEFT, NONE;

	public boolean isOpposite(Move move) {
		switch (move) {
		case DOWN:
			return this == UP;
		case UP:
			return this == DOWN;
		case LEFT:
			return this == RIGHT;
		case RIGHT:
			return this == LEFT;
		default:
			return false;
		}

	}
}
