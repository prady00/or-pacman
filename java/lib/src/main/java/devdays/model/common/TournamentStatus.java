package devdays.model.common;

public enum TournamentStatus {
	WAITING_FOR_PLAYERS, WAIT_ROUND_DEFINITION, PLAYING, FINISHED, INIT_ERROR;
}
