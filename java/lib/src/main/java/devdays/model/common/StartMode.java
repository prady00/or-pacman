package devdays.model.common;

/**
 * Start mode for the tournament.
 * 
 * <li>AUTO_TIMER_30S => Start automatically 30s after first team registration (dev mode) 
 * <li>AUTO_ALL_TEAM => Start automatically when team count has been reached (dev mode)
 * <li>MANUAL => Tournament start is made by an explicit call
 * @author cbtv4480
 *
 */
public enum StartMode {
	AUTO_TIMER_30S, AUTO_ALL_TEAM, MANUAL;
}
