package devdays.model.service;

import java.util.ArrayList;
import java.util.List;

import devdays.model.common.RoundDefinitionMode;
import devdays.model.common.StartMode;
import devdays.model.common.TournamentStatus;

public class TournamentDto {
	private String name;
	private int round;
	private int maxTeamCount;
	private TournamentStatus status;
	private String winningTeam;
	private boolean isDebugMode;
	private RoundDefinitionMode roundDefinitionMode;
	private ArrayList<String> mapSequence = new ArrayList<String>();

	private ArrayList<TeamDto> registeredTeams = new ArrayList<TeamDto>();

	private ArrayList<String> allowedTeams = new ArrayList<String>();

	// TournementPhase - Games
	private ArrayList<RoundDto> rounds = new ArrayList<RoundDto>();

	private StartMode startMode;

	public List<String> getMapSequence() {
		return mapSequence;
	}

	public void setMapSequence(ArrayList<String> mapSequence) {
		this.mapSequence = mapSequence;
	}

	public TournamentStatus getStatus() {
		return status;
	}

	public void setStatus(TournamentStatus status) {
		this.status = status;
	}

	public String getWinningTeam() {
		return winningTeam;
	}

	public void setWinningTeam(String winningTeam) {
		this.winningTeam = winningTeam;
	}

	public StartMode getStartMode() {
		return startMode;
	}

	public void setStartMode(StartMode startMode) {
		this.startMode = startMode;
	}

	public int getMaxTeamCount() {
		return maxTeamCount;
	}

	public void setMaxTeamCount(int maxTeamCount) {
		this.maxTeamCount = maxTeamCount;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getRound() {
		return round;
	}

	public void setRound(int round) {
		this.round = round;
	}

	public ArrayList<TeamDto> getRegisteredTeams() {
		return registeredTeams;
	}

	public void setRegisteredTeams(ArrayList<TeamDto> teams) {
		this.registeredTeams = teams;
	}

	public ArrayList<RoundDto> getRounds() {
		return rounds;
	}

	public void setRounds(ArrayList<RoundDto> rounds) {
		this.rounds = rounds;
	}

	public boolean isDebugMode() {
		return isDebugMode;
	}

	public void setDebugMode(boolean isDebugMode) {
		this.isDebugMode = isDebugMode;
	}

	public ArrayList<String> getAllowedTeams() {
		return allowedTeams;
	}

	public void setAllowedTeams(ArrayList<String> allowedTeams) {
		this.allowedTeams = allowedTeams;
	}

	public RoundDefinitionMode getRoundDefinitionMode() {
		return roundDefinitionMode;
	}

	public void setRoundDefinitionMode(RoundDefinitionMode roundDefinitionMode) {
		this.roundDefinitionMode = roundDefinitionMode;
	}

}
