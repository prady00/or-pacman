package devdays.model.service;

public class TeamDto {
	private int score;
	private String teamName;
	private PlayerDto ghost;
	private PlayerDto pacman;

	public TeamDto() {
	}

	public TeamDto(String teamName, PlayerDto ghost, PlayerDto pacman) {
		super();
		this.teamName = teamName;
		this.ghost = ghost;
		this.pacman = pacman;
	}

	public void setGhost(PlayerDto ghost) {
		this.ghost = ghost;
	}

	public PlayerDto getGhost() {
		return ghost;
	}

	public void setPacman(PlayerDto pacman) {
		this.pacman = pacman;
	}

	public PlayerDto getPacman() {
		return pacman;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

	public String getTeamName() {
		return teamName;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public int getScore() {
		return score;
	}

}
