package devdays.model.service;

import devdays.model.common.PlayerStatus;

public class PlayerDto {
	private String name;

	private PlayerStatus status = PlayerStatus.EXPECTED;

	public PlayerDto() {
	}

	public PlayerDto(String playerName, PlayerStatus status) {
		this.name = playerName;
		this.status = status;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setStatus(PlayerStatus status) {
		this.status = status;
	}

	public PlayerStatus getStatus() {
		return status;
	}

}
