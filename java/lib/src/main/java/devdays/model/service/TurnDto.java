package devdays.model.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import devdays.model.common.ScoreEvent;

public class TurnDto {
	private int turnNumber;

	/**
	 * Map at the beginning of the turn 
	 */
	private char[][] map;

	private boolean isLastTurn;

	/**
	 * Events once the turn is finished (end move computed)
	 */
	private ArrayList<ScoreEvent> events = new ArrayList<ScoreEvent>();

	/**
	 * Teams state at the beginning of the round
	 */
	private ArrayList<TeamTurnDataDto> teams = new ArrayList<TeamTurnDataDto>();

	public boolean isLastTurn() {
		return isLastTurn;
	}

	public void setLastTurn(boolean isLastTurn) {
		this.isLastTurn = isLastTurn;
	}

	public int getTurnNumber() {
		return turnNumber;
	}

	public void setNumber(int turnNumber) {
		this.turnNumber = turnNumber;
	}

	public void setMap(char[][] map) {
		this.map = map;
	}

	public char[][] getMap() {
		return map;
	}

	@Override
	public String toString() {
		StringBuffer result = new StringBuffer();

		for (int line = 0; line < map.length; line++) {
			char[] curLine = map[line];

			result.append(line).append(" ");

			for (int column = 0; column < curLine.length; column++) {
				char cell = curLine[column];
				result.append(cell);
				result.append(" ");
			}
			result.append(" ").append(line).append("\n");
		}

		result.append("team:");
		Iterator<TeamTurnDataDto> iterTeams = teams.iterator();
		while (iterTeams.hasNext()) {
			TeamTurnDataDto entry = iterTeams.next();
			result.append(entry.getPacman().getName()).append(" ").append(entry.getPacman().getPosition()).append(" ");
			result.append(entry.getGhost().getName()).append(" ").append(entry.getGhost().getPosition()).append(" ");
			result.append("score: ").append(entry.getScore());
			result.append("\n");
		}

		return super.toString();
	}

	public ArrayList<ScoreEvent> getEvents() {
		return events;
	}

	public void setEvents(ArrayList<ScoreEvent> events) {
		this.events = events;
	}

	public List<TeamTurnDataDto> getTeams() {
		return teams;
	}

	public void setTeams(ArrayList<TeamTurnDataDto> teams) {
		this.teams = teams;
	}

}
