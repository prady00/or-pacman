package devdays.model.service;

import devdays.model.common.Move;

public class PlayerTurnDataDto {
	private String name;
	private PositionDto position;
	private Move move;
	private boolean isDead;
	private boolean isSuperMode;
	private boolean isInvincible;

	public String getName() {
		return name;
	}

	public void setName(String playerName) {
		this.name = playerName;
	}

	public PlayerTurnDataDto(int i, int j, Move move) {
		position = new PositionDto(i, j);
		this.move = move;
	}

	public PlayerTurnDataDto(PositionDto position, Move move) {
		this.position = position;
		this.move = move;
	}

	public PlayerTurnDataDto() {
	}

	public void setPosition(PositionDto position) {
		this.position = position;
	}

	public PositionDto getPosition() {
		return position;
	}

	public void setMove(Move move) {
		this.move = move;
	}

	public Move getMove() {
		return move;
	}

	public void setPosition(int line, int column) {
		position = new PositionDto(line, column);
	}

	public boolean isDead() {
		return isDead;
	}

	public void setDead(boolean isDead) {
		this.isDead = isDead;
	}

	public boolean isSuperMode() {
		return isSuperMode;
	}

	public void setSuperMode(boolean isSuperMode) {
		this.isSuperMode = isSuperMode;
	}

	public boolean isInvincible() {
		return isInvincible;
	}

	public void setInvincible(boolean isInvincible) {
		this.isInvincible = isInvincible;
	}

}
