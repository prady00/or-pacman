package devdays.model.service;

import java.util.ArrayList;
import java.util.List;

public class RoundDto {
	private int id;
	private List<GameDto> games = new ArrayList<GameDto>();
	private boolean isFinished;
	private boolean isPool;

	public List<GameDto> getGames() {
		return games;
	}

	public void setGames(List<GameDto> games) {
		this.games = games;
	}

	public boolean isFinished() {
		return isFinished;
	}

	public void setFinished(boolean isFinished) {
		this.isFinished = isFinished;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean isPool() {
		return isPool;
	}

	public void setPool(boolean isPool) {
		this.isPool = isPool;
	}

}
