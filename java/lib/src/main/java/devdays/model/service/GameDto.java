package devdays.model.service;

import java.util.List;

import devdays.model.common.GameStatus;

public class GameDto {

	private String winningTeam;

	private String id;

	private int expectedTeamCount;

	private List<TeamDto> teams;

	private Integer currentTurn;

	private GameStatus gameStatus;

	private String mapName;

	public String getWinningTeam() {
		return winningTeam;
	}

	public void setWinningTeam(String winningTeam) {
		this.winningTeam = winningTeam;
	}

	public int getExpectedTeamCount() {
		return expectedTeamCount;
	}

	public void setExpectedTeamCount(int expectedTeamCount) {
		this.expectedTeamCount = expectedTeamCount;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<TeamDto> getTeams() {
		return teams;
	}

	public void setTeams(List<TeamDto> expectedTeams) {
		this.teams = expectedTeams;
	}

	public GameStatus getGameStatus() {
		return gameStatus;
	}

	public void setGameStatus(GameStatus gameStatus) {
		this.gameStatus = gameStatus;
	}

	public void setCurrentTurn(Integer currentTurn) {
		this.currentTurn = currentTurn;
	}

	public Integer getCurrentTurn() {
		return currentTurn;
	}

	public String getMapName() {
		return mapName;
	}

	public void setMapName(String mapName) {
		this.mapName = mapName;
	}

}
