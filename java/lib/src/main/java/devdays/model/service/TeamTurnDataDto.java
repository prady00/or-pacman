package devdays.model.service;

public class TeamTurnDataDto {
	private String teamName;
	private int score;
	PlayerTurnDataDto ghost;
	PlayerTurnDataDto pacman;

	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public PlayerTurnDataDto getGhost() {
		return ghost;
	}

	public void setGhost(PlayerTurnDataDto ghost) {
		this.ghost = ghost;
	}

	public PlayerTurnDataDto getPacman() {
		return pacman;
	}

	public void setPacman(PlayerTurnDataDto pacman) {
		this.pacman = pacman;
	}

}
