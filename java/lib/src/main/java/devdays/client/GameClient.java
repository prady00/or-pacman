package devdays.client;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeoutException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import devdays.model.common.GameStatus;
import devdays.model.common.Move;
import devdays.model.common.RoundDefinitionMode;
import devdays.model.common.StartMode;
import devdays.model.common.TournamentStatus;
import devdays.model.service.GameDto;
import devdays.model.service.TournamentDto;
import devdays.model.service.TurnDto;

public class GameClient {

	private static final Log logger = LogFactory.getLog(GameClient.class);

	protected RestTemplate restTemplate = new RestTemplate();
	protected String serverUrl;
	private String cookie;

	public GameClient(String serverUrl) {
		this.serverUrl = serverUrl;
	}

	public String getServerUrl() {
		return serverUrl;
	}

	public void login(String user, String password) {
		Map<String, String> params = new HashMap<String, String>();

		params.put("user", user);
		params.put("password", password);

		// restTemplate.postForLocation(, "", params);

		HttpHeaders requestHeaders = new HttpHeaders();
		// requestHeaders.add("Cookie", "JSESSIONID=" + cookie);
		HttpEntity requestEntity = new HttpEntity(null, requestHeaders);
		HttpEntity<String> response = restTemplate.exchange(serverUrl + "/login?user={user}&password={password}",
				HttpMethod.GET, requestEntity, String.class, params);

		// Retrieve cookies from response headers
		HttpHeaders headers = response.getHeaders();
		cookie = headers.getFirst("Set-Cookie");

		logger.debug("cookie = " + cookie);
	}

	public void logout() {
		cookie = null;
		restTemplate.postForLocation(serverUrl + "/logout", null);
	}

	protected void exchangeWithCookies(String url, Map<String, ?> params, Object bodyObject, HttpMethod method) {
		HttpHeaders requestHeaders = new HttpHeaders();
		if (cookie != null) {
			requestHeaders.add("Cookie", cookie);
		}
		HttpEntity<?> requestEntity = new HttpEntity(bodyObject, requestHeaders);
		restTemplate.exchange(url, method, requestEntity, String.class, params);
	}

	protected void exchangeWithCookies(String url, Map<String, String> params, HttpMethod method) {
		HttpHeaders requestHeaders = new HttpHeaders();
		if (cookie != null) {
			requestHeaders.add("Cookie", cookie);
		}
		HttpEntity<String> requestEntity = new HttpEntity<String>(null, requestHeaders);
		restTemplate.exchange(url, method, requestEntity, String.class, params);

	}

	protected <T> T exchangeWithCookies(String url, Map<String, ?> params, HttpMethod method, Class<T> responseType) {
		HttpHeaders requestHeaders = new HttpHeaders();
		if (cookie != null) {
			requestHeaders.add("Cookie", cookie);
		}
		HttpEntity<String> requestEntity = new HttpEntity<String>(null, requestHeaders);
		ResponseEntity<T> response = restTemplate.exchange(url, method, requestEntity, responseType, params);

		return response.getBody();
	}

	public void registerInGame(String playerName, String gameName) {

		Map<String, String> params = new HashMap<String, String>();

		params.put("playerName", playerName);
		params.put("gameId", gameName);

		exchangeWithCookies(serverUrl + "/players/{playerName}/game/{gameId}", params, HttpMethod.POST);

	}

	public void registerInTournament(String tournamentName, String teamName, String pacmanName, String ghostName) {

		Map<String, String> params = new HashMap<String, String>();

		params.put("tournamentName", tournamentName);
		params.put("teamName", teamName);
		params.put("pacmanName", pacmanName);
		params.put("ghostName", ghostName);

		exchangeWithCookies(serverUrl
				+ "/tournaments/{tournamentName}/team/{teamName}?pacmanName={pacmanName}&ghostName={ghostName}",
				params, HttpMethod.POST);

	}

	/**
	 * Create a tournament with default mode (Start automatically maxTeamCount
	 * teams have registred on tournament). DEBUG PURPOSE
	 * 
	 * @param tournamentName
	 * @param maxTeamCount
	 * @param startMode
	 */
	public void createTournamentWithMap(String tournamentName, int maxTeamCount, StartMode startMode,
			ArrayList<String> maps) {

		Map<String, Object> params = new HashMap<String, Object>();

		params.put("tournamentName", tournamentName);

		TournamentDto tournament = new TournamentDto();
		tournament.setName(tournamentName);
		tournament.setStartMode(startMode);
		tournament.setMaxTeamCount(maxTeamCount);
		tournament.setStartMode(startMode);
		tournament.setDebugMode(false);
		tournament.setMapSequence(maps);

		exchangeWithCookies(serverUrl + "/tournaments/{tournamentName}", params, tournament, HttpMethod.POST);

	}

	/**
	 * Create a tournament with default mode (Start automatically maxTeamCount
	 * teams have registred on tournament). DEBUG PURPOSE
	 * 
	 * @param tournamentName
	 * @param maxTeamCount
	 * @param startMode
	 */
	public void createPrivateTournament(String tournamentName, int maxTeamCount, StartMode startMode,
			ArrayList<String> allowedTeamNames) {

		Map<String, Object> params = new HashMap<String, Object>();

		params.put("tournamentName", tournamentName);

		TournamentDto tournament = new TournamentDto();
		tournament.setName(tournamentName);
		tournament.setStartMode(startMode);
		tournament.setMaxTeamCount(maxTeamCount);
		tournament.setStartMode(startMode);
		tournament.setDebugMode(false);
		tournament.setAllowedTeams(allowedTeamNames);

		exchangeWithCookies(serverUrl + "/tournaments/{tournamentName}", params, tournament, HttpMethod.POST);

	}

	/**
	 * Create a tournament for -=DEBUG PURPOSE=-
	 * 
	 * @param tournamentName
	 * @param maxTeamCount
	 * @param startMode
	 */
	public void createDefaultTournament(String tournamentName) {

		Map<String, Object> params = new HashMap<String, Object>();

		params.put("tournamentName", tournamentName);

		TournamentDto tournament = new TournamentDto();

		exchangeWithCookies(serverUrl + "/tournaments/{tournamentName}", params, tournament, HttpMethod.POST);

	}

	/**
	 * Create a tournament for -=DEBUG PURPOSE=-
	 * 
	 * @param tournamentName
	 * @param maxTeamCount
	 * @param startMode
	 */
	public void createTournament(String tournamentName, int maxTeamCount) {

		Map<String, Object> params = new HashMap<String, Object>();

		params.put("tournamentName", tournamentName);

		TournamentDto tournament = new TournamentDto();
		tournament.setName(tournamentName);
		tournament.setStartMode(StartMode.AUTO_ALL_TEAM);
		tournament.setMaxTeamCount(maxTeamCount);
		tournament.setDebugMode(false);
		tournament.setRoundDefinitionMode(RoundDefinitionMode.AUTO);

		exchangeWithCookies(serverUrl + "/tournaments/{tournamentName}", params, tournament, HttpMethod.POST);

	}

	/**
	 * Create a tournament for -=DEBUG PURPOSE=-
	 * 
	 * @param tournamentName
	 * @param maxTeamCount
	 * @param startMode
	 */
	public void createTournament(String tournamentName, int maxTeamCount, StartMode startMode,
			RoundDefinitionMode roundDefinitionMode) {

		Map<String, Object> params = new HashMap<String, Object>();

		params.put("tournamentName", tournamentName);

		TournamentDto tournament = new TournamentDto();
		tournament.setName(tournamentName);
		tournament.setStartMode(startMode);
		tournament.setMaxTeamCount(maxTeamCount);
		tournament.setDebugMode(false);
		tournament.setRoundDefinitionMode(roundDefinitionMode);

		exchangeWithCookies(serverUrl + "/tournaments/{tournamentName}", params, tournament, HttpMethod.POST);

	}

	/**
	 * Create a tournament for -=DEBUG PURPOSE=-
	 * 
	 * @param tournamentName
	 * @param maxTeamCount
	 * @param startMode
	 */
	public TournamentDto getTournament(String tournamentName) {

		Map<String, Object> params = new HashMap<String, Object>();

		params.put("tournamentName", tournamentName);

		return exchangeWithCookies(serverUrl + "/tournaments/{tournamentName}", params, HttpMethod.GET,
				TournamentDto.class);
	}

	/**
	 * ask a game to the server. (long polling)
	 * @param tournamentName
	 * @param teamName
	 * @return
	 */
	public GameDto getNextGameInTournament(String tournamentName, String teamName) {

		Map<String, String> params = new HashMap<String, String>();

		params.put("tournamentName", tournamentName);
		params.put("teamName", teamName);

		return exchangeWithCookies(serverUrl + "/tournaments/{tournamentName}/team/{teamName}/game", params,
				HttpMethod.GET, GameDto.class);

	}

	public void playAturn(String playerName, String gameId, int turnId, Move move) {

		Map<String, String> params = new HashMap<String, String>();

		params.put("playerName", playerName);
		params.put("gameId", gameId);
		params.put("turnId", "" + turnId);
		params.put("move", "" + move);

		exchangeWithCookies(serverUrl + "/players/{playerName}/game/{gameId}/turn/{turnId}/move/{move}", params,
				HttpMethod.POST);

	}

	public GameDto getGame(String gameName) {

		Map<String, String> params = new HashMap<String, String>();
		params.put("gameName", gameName);

		GameDto result = exchangeWithCookies(serverUrl + "/games/{gameName}", params, HttpMethod.GET, GameDto.class);
		return result;
	}

	/**
	 * Get a turn description. (long polling)
	 * @param gameName
	 * @param turnNum
	 * @return
	 */
	public TurnDto getTurn(String gameName, int turnNum) {
		Map<String, String> params = new HashMap<String, String>();

		params.put("turnId", turnNum + "");
		params.put("gameName", gameName);

		return exchangeWithCookies(serverUrl + "/games/{gameName}/turn/{turnId}", params, HttpMethod.GET, TurnDto.class);
	}

	public GameDto waitTournamentGameAvailable(String tournamentName, String teamName) throws TimeoutException {
		GameDto game = null;

		try {
			game = getNextGameInTournament(tournamentName, teamName);
		} catch (HttpClientErrorException e) {
			logger.info("Game not found " + tournamentName + "/" + teamName);
		}
		int tryCount = 0;
		while (tryCount < 10 && game == null) {
			try {
				Thread.sleep(200);
			} catch (InterruptedException e) {
			}
			try {
				game = getNextGameInTournament(tournamentName, teamName);
			} catch (HttpClientErrorException e) {
				if (e.getStatusCode() != HttpStatus.NOT_FOUND) {
					throw e;
				}
				logger.info("Game not found: " + tournamentName + "/" + teamName);
			}

			tryCount++;
		}

		if (tryCount == 10) {
			throw new TimeoutException("Unable to get a game from tournament " + tournamentName + " for team "
					+ teamName);
		}

		logger.debug("Tournament : " + tournamentName + " next Game ready = " + game.getId());

		return game;

	}

	public GameDto waitGameStatusChange(final String gameName, GameStatus initialStatus) throws TimeoutException {

		GameDto game = getGame(gameName);

		int tryCount = 0;
		while (game.getGameStatus() == initialStatus && tryCount < 20) {
			logger.debug("Game " + game.getId() + " status = " + game.getGameStatus() + " initial = " + initialStatus);
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
			}
			game = getGame(gameName);
			tryCount++;
		}

		if (tryCount == 20) {
			throw new TimeoutException("Unable to get game " + gameName);
		}

		logger.debug("Game " + game.getId());

		return game;
	}

	public TurnDto waitTurnCreation(final String gameName, int turnNum) throws TimeoutException {
		TurnDto turn = null;
		logger.debug("Waiting game:" + gameName + " - turn:" + turnNum);
		try {
			turn = getTurn(gameName, turnNum);
		} catch (HttpClientErrorException e) {
			logger.info("Turn not found game " + gameName + " - turn " + turnNum);
		}
		int tryCount = 0;
		while (tryCount < 3 && turn == null) {
			try {
				turn = getTurn(gameName, turnNum);
			} catch (HttpClientErrorException e) {
				if (e.getStatusCode() != HttpStatus.NOT_FOUND) {
					throw e;
				}
				logger.info("Turn not found game " + gameName + " - turn " + turnNum);
			}

			tryCount++;
		}

		if (tryCount == 3) {
			throw new TimeoutException("Unable to get turn " + turnNum);
		}

		logger.debug("Turn found game:" + gameName + " - turn:" + turnNum);

		return turn;
	}

	public void abortTournament(String tournamentName) {
		Map<String, String> params = new HashMap<String, String>();

		params.put("tournamentName", tournamentName);

		exchangeWithCookies(serverUrl + "/tournaments/{tournamentName}/abort", params, HttpMethod.POST);

	}

	public void resetTournament(String tournamentName) {
		Map<String, String> params = new HashMap<String, String>();

		params.put("tournamentName", tournamentName);

		exchangeWithCookies(serverUrl + "/tournaments/{tournamentName}/reset", params, HttpMethod.POST);

	}

	public void waitTournamentStatusChange(String tournamentName, TournamentStatus initialStatus)
			throws TimeoutException {
		TournamentDto tournament = getTournament(tournamentName);

		int tryCount = 0;
		int MAX_TRY_COUNT = 40;
		while (tournament.getStatus() == initialStatus && tryCount < MAX_TRY_COUNT) {

			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
			}
			tournament = getTournament(tournamentName);
			tryCount++;
		}

		if (tryCount == MAX_TRY_COUNT) {
			throw new TimeoutException("Timeout : tournament still in status " + initialStatus);
		} else {
			logger.debug("Tournament " + tournamentName + " : state changed " + initialStatus + "->"
					+ tournament.getStatus());
		}

	}

	public void waitForTournamentStatus(String tournamentName, TournamentStatus expectedStatus) throws TimeoutException {
		TournamentDto tournament = getTournament(tournamentName);

		int tryCount = 0;
		int MAX_TRY_COUNT = 30;
		while (tournament.getStatus() != expectedStatus && tryCount < MAX_TRY_COUNT) {
			logger.debug("Game " + tournament.getName() + " status = " + tournament.getStatus() + " expected = "
					+ expectedStatus);
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
			}
			tournament = getTournament(tournamentName);
			tryCount++;
		}

		if (tryCount >= MAX_TRY_COUNT) {
			throw new TimeoutException("Timeout : tournament not in status " + expectedStatus);
		}

	}

	public void deleteTournament(String tournamentName) {
		Map<String, String> params = new HashMap<String, String>();

		params.put("tournamentName", tournamentName);

		exchangeWithCookies(serverUrl + "/tournaments/{tournamentName}", params, HttpMethod.DELETE);
	}
}
